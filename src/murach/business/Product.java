package murach.business;

import java.text.NumberFormat;

/**
 * As with similar examples we have seen previously, this class 
 * is used to instantiate Product objects; there's nothing in this 
 * class that has anything to do with connecting to a database!
 * 
 * This class represents the "business" tier" of the application architecture;
 * it defines "business objects" and "business methods" (limited as they
 * may be)
 * 
 * @author pmize@email.uscb.edu
 * @version ICE for 23 Jan 2017
 */
public class Product {
    private long id;
    private String code;
    private String description;
    private double price;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String name) {
        this.description = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    
    public String getPriceFormatted() {
        NumberFormat currencyFormatter =
                NumberFormat.getCurrencyInstance();
        return currencyFormatter.format(getPrice());
    }    
}
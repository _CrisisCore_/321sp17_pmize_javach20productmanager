package murach.db;

/*
What do each of these classes/packages do?
*/

import java.sql.Connection;     // used to instantiate new Connection objects
import java.sql.DriverManager;  // contains a "getConnection" method (which 
                                // itself is called as part of the getConnection
                                // method defined for *this* class)
import java.sql.SQLException;   // used to handle DB connection exceptions

/**
 * @author pmize@email.uscb.edu
 * @version ICE for 23 Jan 2017
 */
public class DBUtil {
    
    /* 
    First, declare a static variable for the Connection object
    (we're not instantiating any DBUtil objects, but the DBUtil class itself
    can still have static fields that refer to "utility objects" used
    by the class as a whole!)
    */
    private static Connection connection;
    
    /*
    Provide a private constructor that prevents other classes from
    creating an object in the DBUtil class. 
    
    In other words, this will eliminate the possibility of a (public!) 
    default constructor being created by the JDK compiler; this is perfectly 
    appropriate because the DBUtil class only defines static methods and 
    static fields; there are no instance methods or instance variables!
    */
    private DBUtil() {} //eliminates possibility of default constructor being
                        //called

    /*
    Before you can retrieve or modify the data in a database, you need
    to connect to it. To do that, you use the getConnection method of
    the DriverManager class (part of the java.sql package) to return
    a Connection object [we will eventually see a similar object-oriented
    approach when we develop PHP applications that connect to a MySQL DB]
    
    Also: it's declared as synchronized because we want each method to finish  
    before we can call another method (e.g., we want getConnection
    to finish executing BEFORE we call the closeConnection method, defined
    below)
    */
    public static synchronized Connection getConnection() throws DBException {
        
        /* 
        Has a connection already been opened? If so, just use that connection...
        */
        if (connection != null) {
            return connection;
        }
        else /* otherwise, open a connection by creating a Connection object */
        {
            try {
                // set the db url, username, and password
                // jdbc = Java DataBase Connectivity
                String url = "jdbc:mysql://localhost:3306/mma";
                String username = "mma_user";
                String password = "sesame";

                // get and return connection
                connection = DriverManager.getConnection(
                        url, username, password);
                return connection;
            } catch (SQLException e) {
                throw new DBException(e);
            }            
        }
    }
    
    public static synchronized void closeConnection() throws DBException {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                throw new DBException(e);
            } finally {
                connection = null;                
            }
        }
    }
}